import java.util.LinkedList;
import java.util.List;

public class DataBase {
    private final Customer[] buyers;
    private int length;

    public DataBase() {
        this.buyers = new Customer[6];
        this.length = 0;
    }
    public void sortInAlphabeticalOrder(){
        boolean isSorted = false;
        Customer buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < buyers.length-1; i++) {
                if(buyers[i].getSurname().compareTo(buyers[i+1].getSurname()) > 0){
                    isSorted = false;

                    buf = buyers[i];
                    buyers[i] = buyers[i+1];
                    buyers[i+1] = buf;
                }
            }
        }

    }
    public void display(){
        for (int i = 0; i < 5; i++){
            System.out.println(buyers[i]);
        }
    }
    public List<Customer> customersWithCardNumberInTheInterval(String min, String max){
        List<Customer> customersInTheInterval = new LinkedList<>();
        for (int i = 0; i < 6; i++){
            if (buyers[i].getCreditCardNumber().compareTo(min) >= 0 && buyers[i].getCreditCardNumber().compareTo(max) <= 0){
                customersInTheInterval.add(buyers[i]);
            }
        }
        return customersInTheInterval;
    }
    public void add(Customer a){
        buyers[length] = a;
        length++;
    }
}
