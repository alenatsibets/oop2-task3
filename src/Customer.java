public class Customer {
    private final int id;
    private final String surname;
    private final String firstName;
    private final String patronymic;
    private final String address;
    private final String creditCardNumber;
    private final int bankAccountNumber;

    public Customer(int id, String surname, String firstName, String patronymic, String address, String creditCardNumber, int bankAccountNumber) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.address = address;
        this.creditCardNumber = creditCardNumber;
        this.bankAccountNumber = bankAccountNumber;
    }
    public String getSurname() {
        return surname;
    }
    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", address='" + address + '\'' +
                ", creditCardNumber=" + creditCardNumber +
                ", bankAccountNumber=" + bankAccountNumber +
                '}';
    }
}
