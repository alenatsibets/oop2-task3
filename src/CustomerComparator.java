import java.util.Comparator;

public class CustomerComparator implements Comparator<Customer> {
    @Override
    public int compare(Customer o1, Customer o2) {
        String cardNumber1 = o1.getCreditCardNumber();
        String cardNumber2 = o2.getCreditCardNumber();
        return cardNumber1.compareTo(cardNumber2);
    }
}
