import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DataBase base = new DataBase();
        base.add(new Customer(12345678, "Malevich", "Kazimir", "Severinovich", "711-2880 Nulla St.", "3742_4545_5400_1268", 1001001234));
        base.add(new Customer(81234567, "Lissitzki", "Kazimir", "Markovich", "606-3727 Ullamcorper. Street", "3782_8224_6310_0051", 1001002341));
        base.add(new Customer(78123456, "Chagall", "Marc", "Zaharovich", "7292 Dictum Av.", "6250_9410_0652_8599", 1001003412));
        base.add(new Customer(67812345, "Pen", "Yehuda ", "Moiseevich", "191-103 Integer Rd.", "6011_5564_4857_8945", 1001004123));
        base.add(new Customer(56781234, "Khidekel ", "Lazar ", "Markovich", "5543 Aliquet St.", "2222_4200_0000_1113", 1001001235));
        base.add(new Customer(45678123, "Suetin", "Nikolai ", "Mikhailovich", "6351 Fringilla Avenue", "4263_9826_4026_9299", 1001004523));

        System.out.println("Enter 1 to sort customers in alphabetical order");
        System.out.println("Enter 2 to see a list of customers whose credit card number is in the specified interval");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        if (number == 1){
            base.sortInAlphabeticalOrder();
            base.display();
        } else if (number == 2) {
            System.out.println("Enter a min credit card number:");
            String min = sc.nextLine();
            System.out.println("Enter a max credit card number:");
            String max = sc.nextLine();
            List<Customer> customersInTheInterval = base.customersWithCardNumberInTheInterval(min, max);
            customersInTheInterval.sort(new CustomerComparator());
            for (Customer element: customersInTheInterval) {
                System.out.println(element);
            }
        } else {
            throw new RuntimeException("Incorrect value");
        }
    }
}